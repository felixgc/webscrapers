import scrapy
import re

class DesignerWardrobeSpider(scrapy.Spider):
    """ Designerwardrobe.com fails to load more dresses after 21 pages, 
    going to page 22 results in an empty page"""

    name = 'designerwardrobe'
    start_urls = [
        'https://designerwardrobe.com.au/shop?categories=women,womens-dresses&type=designer'
    ]
    current_page = 1
    MAX_PAGES = 22 # From testing

    def parse(self, response):
        """Handle the response downloaded fo reach of the requests.
        Returns 'TestResponse' that holds page content + further methods
        Should find new urls to follow and creates new requests from them."""
        self.current_page += 1

        # Select all items with rental badge
        for item in response.xpath('//div[@class="brick-inner" and ./div/span//text() = "RENTAL"]'):
            yield {
                'item_link': response.urljoin(item.css('.img-wrapper > a::attr(href)').extract_first()),
                'brand_name': item.css('.brand-name::text').extract_first(),
                'item_name': item.css('.title::text').extract_first(),
                'rental_price': item.css('.price::text').extract_first()[1:],
                'sizes': item.css('.size::text').extract_first()[6:8],
                'likes': item.css('.like > span::text').extract_first()
            }

        if(self.current_page <= self.MAX_PAGES):
            next_page = response.urljoin('?page=' + str(self.current_page))
            yield response.follow(next_page, callback=self.parse)
            