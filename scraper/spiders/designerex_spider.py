import scrapy
import re

class DesignerExSpider(scrapy.Spider):
    name = 'designerex'
    start_urls = [
        'https://designerex.com.au/search'
    ]

    def parse_item(self, response):
        sizes = response.css('#reservation_size::attr(value)').extract_first()
        if sizes is None:
            sizes = ' '.join(response.css('#reservation_size > option::attr(value)')[1:].extract())

        yield {
            'item_link': response.url,
            'brand_name': response.css('#dress_brand::text').extract_first(),
            'item_name': response.css('.dress_name::text').extract_first(), 
            'rental_price': response.css('.dress_rental_price::text').extract_first(),
            'rrp': response.css('.rrp::text').extract_first()[5:],
            'sizes': sizes,
            'lender_link': response.urljoin(response.css('#profile_image::attr(href)').extract_first()),
            'lender_rating':response.css('.review_verified > span::attr(data-content)').extract_first(),
            'additional': str(response.css('.sl-content::text').extract())
        }

    def parse(self, response):
        """Handle the response downloaded fo reach of the requests.
        Returns 'TestResponse' that holds page content + further methods
        Should find new urls to follow and creates new requests from them."""

        for href in response.css('.item > a::attr(href)'):
            yield response.follow(href, self.parse_item)

        next_page = response.xpath('//a[contains(@rel, "next")]').xpath('@href').extract_first()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)
            