import scrapy
import re

class RentADressAustraliaSpider(scrapy.Spider):
    """Required active cookies to work"""

    name = 'rentadressaustralia'
    start_urls = [
        'http://rentadressaustralia.com/browse/Dresses/'
    ]

    def parse_item(self, response):
        yield {
            'item_link': response.url,
            'brand_name': response.css('.Brand.fieldValue::text').extract_first()[2:-3],
            'item_name': response.css('.Title.fieldValue::text').extract_first()[2:-3], 
            'rental_price': response.css('.Price > .fieldValuePrice::text').extract_first()[1:],
            'rrp': response.css('.RRP > .fieldValuePrice::text').extract_first()[1:],
            'sizes': response.css('.Size.fieldValue::text').extract_first()[2:-3],
            'raing': response.css('.toolTipBody::text').extract_first()[3:-2] 
            
        }

    def parse(self, response):
        """Handle the response downloaded fo reach of the requests.
        Returns 'TestResponse' that holds page content + further methods
        Should find new urls to follow and creates new requests from them."""

        for href in response.css('.thumbnail > .image > a::attr(href)'):
            yield response.follow(href, self.parse_item)

        next_page = response.css('.nextPageSelector::attr(href)').extract_first().split()
        next_page = ''.join(next_page)
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)
            