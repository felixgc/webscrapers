import scrapy

class TheVolteSpider(scrapy.Spider):
    """The Volte website produces incorrect links in their catalogue on some pages,
    a query link is tied to the items in the catalogue instead."""

    name = 'thevolte'
    start_urls = [
        'https://www.thevolte.com/search/Dress/'
    ]
    current_page = 1
    MAX_SEARCH_PAGES = 340 # get this from looking at website

    def parse_item(self, response):

        rrp = None, 
        rental_price = None
        try:
            rrp = response.css('.retail-value > dd::text').extract_first().split(' ')[0][1:]
            rental_price = response.css('.price > dd')[-1].xpath('./text()').extract_first().split(' ')[0][1:],
        except (AttributeError, IndexError):
            print('Error on page, could be somthing wrong with href: {}'.format(response.url))


        yield {
            'item_link': response.url,
            'brand_name': response.xpath('//dt[contains(.//text(), "Designer")]/following-sibling::dd//text()').extract_first(),
            'item_name': response.css('.item-name::text').extract_first(),
            'rental_price': rental_price,
            'rrp': rrp,
            'sizes': response.xpath('//dt[contains(.//text(), "Size")]/following-sibling::dd//text()').extract_first(),
            'lender_link': response.urljoin(response.css('.avatar + h2 > a::attr(href)').extract_first()),
            'lender_rating': response.css('.item-rating::attr(data-rating)').extract_first()
        }

    def parse(self, response):
        """Handle the response downloaded fo reach of the requests.
        Returns 'TestResponse' that holds page content + further methods
        Should find new urls to follow and creates new requests from them."""
        self.current_page += 1


        for href in response.css('.listing-link::attr(href)'):
            yield response.follow(href, self.parse_item)

        if(self.current_page <= self.MAX_SEARCH_PAGES):
            next_page = response.urljoin('?page=' + str(self.current_page))
            yield response.follow(next_page, callback=self.parse)
            