## Webscrapers
Repository for webscrapers used to gather data on the collections
of competing websites. Scrapers have been written in scrapy.

## Running Scrapers
1. Download repository locally
2. cd into directory 'cd /path/to/repo'
3. Launch python environment 'source venv/bin/bash'
4. Select and run a scraper 'scrapy crawl <scraper-id> -o <filename>.csv'

## Current Scrapers 
-  designerex
-  designerwardrobe
-  rentadressaustralia
-  thevolte